function getMotorClaimOtp() {
  ClaimsDataNew.mobilenumber = $("#mobileNo").val();
  loaderToggle(true);
  var reqObj = jsHelper.cloneJson(
    apiConfig["GENERATE_OTP_MOTOR"].getEmptyRequestObj()
  );
  reqObj.p_user_info_obj.stringVal2 = jsHelper.toString(
    ClaimsDataNew.mobilenumber
  );
  $(document).trigger("api.getMotorClaimOtp", {
    uiObj: reqObj,
    cb: function (e, response) {
      try {
        if (e === true) {
          loaderToggle(false);
          $(".render-mobno").text(mobilenumber);
          $(".verify-otp .resend-otp-text").css("pointer-events", "none");

          //Timer Logic
          ClaimsDataNew.timeLeft = 0;
          ClaimsDataNew.timeLeft = 20;
          ClaimsDataNew.timerId = setInterval(countdown, 1000);
          function countdown() {
            if (ClaimsDataNew.timeLeft == -1) {
              clearTimeout(ClaimsDataNew.timerId);
            } else {
              //   $(".some_div").html("");

              ClaimsDataNew.timeLeft < 10
                ? $(".otp-time-text").text("00:" + "0" + ClaimsDataNew.timeLeft)
                : $(".otp-time-text").text("00:" + ClaimsDataNew.timeLeft);

              ClaimsDataNew.timeLeft--;
            }
            if (ClaimsDataNew.timeLeft == 0) {
              $(".verify-otp .resend-otp-text").css("pointer-events", "unset");
            }
          }
          countdown();

          console.log(response);
        } else {
          console.log("Nothing To Worry");
        }
      } catch (error) {
        console.error(error);
      }
    },
  });
}

// VERFIY OTP
function getMotorClaimValidateOtp() {
  loaderToggle(true);
  // ClaimsDataNew.OTP = $('').val();
  ClaimsDataNew.OTP =
    inputvalue1 + inputvalue2 + inputvalue3 + inputvalue4 + inputvalue5;
  var reqObj = jsHelper.cloneJson(
    apiConfig["VALIDATE_OTP_MOTOR"].getEmptyRequestObj()
  );
  reqObj.p_otp = ClaimsDataNew.OTP;
  reqObj.p_user_id = ClaimsDataNew.mobilenumber;
  $(document).trigger("api.getMotorClaimValidateOtp", {
    uiObj: reqObj,
    cb: function (e, response) {
      try {
        if (e === true) {
          localStorage.setItem(
            "mobNo",
            window.btoa(ClaimsDataNew.mobilenumber)
          );
          localStorage.setItem("Mob", window.btoa(ClaimsDataNew.mobilenumber));
          sessionStorage.setItem(
            "mobNo",
            window.btoa(ClaimsDataNew.mobilenumber)
          );
          sessionStorage.setItem(
            "Mob",
            window.btoa(ClaimsDataNew.mobilenumber)
          );
          GetClmDtls() ; // ADDED BY NM98 11SEPT23
          loaderToggle(false);
        } else {
          showErrorMsg(response.p_error[0].errText);
          loaderToggle(false);
        }
      } catch (error) {
        console.error(error);
      }
    },
  });
}

// GET CLAIM DETAILS
function GetClmDtls() {
  loaderToggle(true);
  var reqObj = jsHelper.cloneJson(apiConfig["GET_MOB_POL_DTLS"].getEmptyRequestObj());
  reqObj.mobPolDtls.stringval1 = ClaimsDataNew.mobilenumber;

  if (getURLParams().mob) {
      ClaimsDataNew.mobilenumber = jsHelper.toString(getURLParams().mob)
      reqObj.mobPolDtls.stringval1 = jsHelper.toString(getURLParams().mob)
  }
  if (getURLParams().polno && getURLParams().polno !== "") {
      reqObj.mobPolDtls.stringval1 = "98"
  }
  if (getURLParams().claimNo && getURLParams().claimNo !== "") {
      reqObj.mobPolDtls.stringval1 = "98"
  }
  if (ClaimsDataNew.claimFlag == "Upload" || ClaimsDataNew.claimFlag == "Status") {
      reqObj.mobPolDtls.stringval2 = "Y"
  }

  $(document).trigger('api.getMotorClmDtls', {

      uiObj: reqObj,
      cb: function (e, response) {
          try {
              if (e === true) {

                  if (response.mobPolDtls[0]) {
                      localStorage.setItem("getclmdetlsResponse", JSON.stringify(response.mobPolDtls));
                      ClaimsDataNew.policyNo = response.mobPolDtls.stringval1;
                      ClaimsDataNew.registerNum = response.mobPolDtls.stringval2;
                      ClaimsDataNew.vehicalname = response.mobPolDtls.stringval3;
                      ClaimsDataNew.vehicalmake = response.mobPolDtls.stringval3;
                      setSessionData("ClaimsDataNew", JSON.stringify(ClaimsDataNew))
                      $('.policy-detail-cards').html('');
                      response.mobPolDtls.forEach(function (carData , index) {
                        $('.policy-detail-cards').append("<div class='policy-card bdr-0062AB36 pos-relative'><p class='policy-no sans-regular-400 c9c9c9c fnt-14px'>Policy no: <span class='cF48221'>"+ carData.stringval1 +
                        "</span></p><h3 class='policy-title sans-bold-700 c000000 fnt-16px'>Car Insurance Policy -Third-party</h3><p class='plcy-exp-date sans-regular-400 c9c9c9c fnt-14px'>Expire on: <span>"+ carData.stringval3+ carData.stringval3 +
                        "</span></p><span class='vehicle-no sans-light-300 c0062AB fnt-16px pos-relative'>"+ carData.stringval3 +
                        "</span><div class='policy-card-btn d-flex justify-sb'><button class='plcy-sub-btn sans-regular-400 ob-none bdr-0062AB c0062AB br-43'>RegisterClaim</button><button class='plcy-sub-btn sans-regular-400 bdr-0062AB bg-0062ab cffffff br-43'>Roadside Assistance</button></div></div>")
                      })

                      ClaimsDataNew.claimFlag = "Register"
                      console.log("OTP Success");
                      $(".home-pg-content").removeClass("dsp-none");
                      $(".policy-claim-content").removeClass("dsp-none");
                      $(".verification").addClass("dsp-none");
                      // $('#claimStepdiv1 p').eq(1).text(ClaimsDataNew.registerNum)
                      // $('#climimgPolicy').text(ClaimsDataNew.policyNo)
                      console.log("getMotorClmDtls");  
                  } 
              } else {
                  if (response.Error = 'Read timed out') {
                      // let timerOn = true;
                      // function timer(remaining) {
                      //     var m = Math.floor(remaining / 60);
                      //     var s = remaining % 60;
                      //     console.log(m + ':' + s);
                      //     remaining -= 1;
                      //     if (remaining >= 0 && timerOn) {
                      //         setTimeout(function () {
                      //             timer(remaining);
                      //         }, 1000);
                      //         return;
                      //     }
                      // sessionStorage.setItem("ClaimsDataNew", JSON.stringify(ClaimsDataNew)); // YW02 24Aug23
                      ClaimsDataNew.updateFlag = "Y"
                      if (!getURLParams().uploadbackbtn) {
                          // $('.policy_claim_div').show();
                      }

                  
                  }
                  if (getURLParams().flag == "REGISTER") {
                      loaderToggle(true)
                  } else {
                      loaderToggle(false)
                  } 

              }
          } catch (error) {
              console.error(error);
          }
      }
  });
}
  
// Add Policy Function
function mobileinputcheck(mobileval){
  var regex = new RegExp("[0-9]{10}");
  if(regex.test(mobileval)){
    // alert("Hide Err mOB nO")
    $(".mobile-num .err-msg").addClass('dsp-none')
    return true
  }else{
    $(".mobile-num .err-msg").removeClass('dsp-none')
    return false;
  }
}

function policyamountcheck(policyamt){
  if(policyamt === ''){
    $(".premium-amnt .err-msg").removeClass('dsp-none')
    return false
  }else{
    $(".premium-amnt .err-msg").addClass('dsp-none')
    return true
  }
}

function emailidpolicy(emailid){
  var regexemail = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
 
  if(emailid.match(regexemail)){
    $(".email-id .err-msg").addClass('dsp-none')
    return true
  }else{
    $(".email-id .err-msg").removeClass('dsp-none')
    return false
  }
}

//OnSubmitpolicy Api Function
function addpolicypopup() {
  loaderToggle(true);
  //claimmotor.newPolicyNo = "OG" + "-" + $('#policyno1').val() + "-" + $('#policyno2').val() + "-" + $('#policyno3').val() + "-" + $('#policyno4').val()
  claimmotor.newPolicyNo = "OG" + "-" + $("#policyNo").val();
  claimmotor.newPremamt = jsHelper.toString($("#pAmnt").val())
  claimmotor.newMobileno = jsHelper.toString($("#mobNum").val())
  claimmotor.newEmail = jsHelper.toString($("#mailID").val())
  var reqObj = jsHelper.cloneJson(apiConfig["GET_MOTOR4WWCMWS"].getEmptyRequestObj());
  reqObj.clmPolicyDtls.stringval12 = claimmotor.newPolicyNo;
  reqObj.clmPolicyDtls.stringval13 = claimmotor.newPremamt;
  reqObj.clmPolicyDtls.stringval14 = claimmotor.newMobileno;
  reqObj.clmPolicyDtls.stringval15 = claimmotor.newEmail;

  $(document).trigger('api.getMotorClmDtls', {
      uiObj: reqObj,
      cb: function (e, response) {
          try {
              if (e === true) {
                loaderToggle(false)
                  // showErrorMsg(response.clmPolicyDtls.stringval15);
                  $(".plcy-added-popup").removeClass('dsp-none');

                  $('.policy-detail-cards').prepend("<div class='policy-card bdr-0062AB36 pos-relative'><p class='policy-no sans-regular-400 c9c9c9c fnt-14px'>Policy no: <span class='cF48221'>"+ response.mobPolDtls[0].stringval1 +
                  "</span></p><h3 class='policy-title sans-bold-700 c000000 fnt-16px'>Car Insurance Policy -Third-party</h3><p class='plcy-exp-date sans-regular-400 c9c9c9c fnt-14px'>Expire on: <span>"+ response.mobPolDtls[0].stringval3+ response.mobPolDtls[0].stringval3 +
                  "</span></p><span class='vehicle-no sans-light-300 c0062AB fnt-16px pos-relative'>"+ response.mobPolDtls[0].stringval3 +
                  "</span><div class='policy-card-btn d-flex justify-sb'><button class='plcy-sub-btn sans-regular-400 ob-none bdr-0062AB c0062AB br-43'>RegisterClaim</button><button class='plcy-sub-btn sans-regular-400 bdr-0062AB bg-0062ab cffffff br-43'>Roadside Assistance</button></div></div>")
                  loaderToggle(false)
              } else {
                  showErrorMsg(response.errorList[0].errtext)
                  loaderToggle(false);

              }
          } catch (error) {
              console.error(error);
          }
      }
  });
}

function showErrorMsg(msg) {
  $('.popupErrorMessage p').html(msg);
  $('.popupErrorMessage, .bodyOverlay').css({
      'display': 'block',
      'z-index': '100'
  });
}

