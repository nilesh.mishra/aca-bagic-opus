var ClaimsDataNew = {};
var mobilenumber;
var claimmotor = {};
loaderToggle(false);
var inputvalue1 = $("#input1").val();
var inputvalue2 = $("#input2").val();
var inputvalue3 = $("#input3").val();
var inputvalue4 = $("#input4").val();
var inputvalue5 = $("#input5").val();
$(document).ready(function () {
  $(".mob-inp").keyup(function () {
    mobilenumber = Number($(this).val());
    var regex = /^\d{3}\d{3}\d{4}$/
    var mobilenumberlength = Number($(this).val().length);
    console.log(mobilenumber);
    if (mobilenumberlength === 10 && regex.test(mobilenumber)) {
      $(".err-msg").addClass("dsp-none");
    } else {
      $(".err-msg").removeClass("dsp-none");
    }
  });

  //OTP BTN
  $(".otp-btn").click(function () {
    $(".sign-in-wrapper").addClass("dsp-none");
    $(".verification").removeClass("dsp-none");
    getMotorClaimOtp();
  });

  //Verify Button
  $(".verify-otp-event").click(function () {
    /* if (
      inputvalue1.length == "" &&
      inputvalue2.length == "" &&
      inputvalue3.length == "" &&
      inputvalue4.length == "" &&
      inputvalue5.length == ""
    ) {
      console.log("Please Enter a valid otp");
    } */ 
      getMotorClaimValidateOtp();
    
  });

  //Resend Click
  $(".resend-otp-text ").click(function () {
    getMotorClaimOtp();
  });

  //Change Number
  $(".change-number").click(function () {
    ClaimsDataNew.timeLeft = 0;
    $(".otp-time-text").text("");
    $(".sign-in-wrapper").removeClass("dsp-none");
    $(".verification").addClass("dsp-none");
  });

    //Policy Tab
  $(".policy-btn-event").click(function () {
    $(this).addClass("btn-active");
    $(".claim-btn-event").removeClass("btn-active");
    $(".claim-btn-wrapper").addClass("dsp-none");
    $(".my-claim").addClass("dsp-none");
  });
  //MyClaim Tab
  $(".claim-btn-event").click(function () {
    $(this).addClass("btn-active");
    $(".policy-btn-event").removeClass("btn-active");
    $(".plcy-clm-input").addClass("dsp-none");
    $(".policy-claim-content").addClass("dsp-none");
  });

  //Add Policy Tab
  $(".add-policy-btn").click(function () {
    $(".home-pg-content").addClass("dsp-none");
    $(".add-policy").removeClass("dsp-none");
  });

  //ADD policy submit button
  $(".add-plcy-btn").click(function () {
    var isvalidateArray = [];
    var policynumber =$("#policyNo").val();
    var policyamount = $("#pAmnt").val();
    var policymobno = $("#mobNum").val();
    var policyemailid = $("#mailID").val();

    var resultmobno = mobileinputcheck(policymobno);
    isvalidateArray.push(resultmobno)
    var resultpolicyamt = policyamountcheck(policyamount)
    isvalidateArray.push(resultpolicyamt)

    var resultemailid = emailidpolicy(policyemailid)
    isvalidateArray.push(resultemailid)

    if(isvalidateArray.includes(false)){
      console.log("No API call")
    }else{
      console.log("API CAlling");
        addpolicypopup()
    }
  });

  //check your policy button
  $(".check-plcy-event").click(function(){
    $(".plcy-added-popup").addClass('dsp-none');
    $(".add-policy").addClass('dsp-none');
    $(".home-pg-content").removeClass('dsp-none')
  })
});

var tabChange = "";
tabChange = function (event, val) {
  var input = document.querySelectorAll(".otp-input");
  var value = event.target.value.replace(/[^0-9]/g, "");
  if (value !== "") {
    if (val < input.length - 1) {
      input[val + 1].focus();
    }
  } else if (val > 0) {
    input[val - 1].focus();
  }
};
