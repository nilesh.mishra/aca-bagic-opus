$(document).on("api.getMotorClaimOtp", function (event, data) {
  var uiObj = data.uiObj;
  var callback = data.cb;
  reqObj = {
    requestJson: uiObj,
  };
  reqObj["headerJson"] = { "Content-Type": "application/json" };

  apiUtility
    .generateOtpMotor(reqObj)
    .then(function (response) {
      if (response.p_error_code == "0") {
        callback(true, response);
      } else {
        callback(false, response);
      }
    })
    .catch(function (err) {
      console.log("Get api.generateOtpMotor Catch listener is triggered");
      callback(false, err);
    });
});

//
$(document).on("api.getMotorClaimValidateOtp", function (event, data) {
  var uiObj = data.uiObj;
  var callback = data.cb;
  reqObj = {
    requestJson: uiObj,
  };

  apiUtility.validateOtpMotor(reqObj)
    .then(function (response) {
      if (
        response.p_error_code == "0" &&
        !response.p_error[0] &&
        response.p_success_msg == null
      ) {
        callback(true, response);
      } else {
        callback(false, response);
      }
    })
    .catch(function (err) {
      console.log(
        "Get api.getMotorClaimValidateOtp Catch listener is triggered"
      );
      callback(false, err);
    });
});

//
$(document).on("api.getMotorClmDtls", function (event, data) {
    var uiObj = data.uiObj;
    var callback = data.cb;
    reqObj = {
        "requestJson": uiObj
    }
   
    apiUtility.getmobpoldtls(reqObj)
        .then(function (response) {
            if (response.errorCode == "0") {
                callback(true, response);
            } else {
                callback(false, response);
            }
        })
        .catch(function (err) {
            console.log("get api.getMotorClmDtlsCatch listener is triggered");
            callback(false, err)
        });
});

//Add Policy Api On

$(document).on("api.getMotorClmDtls", function (event, data) {
  var uiObj = data.uiObj;
  var callback = data.cb;
  reqObj = {
      "requestJson": uiObj
  }
 
  apiUtility.getClmDtls(reqObj)
      .then(function (response) {
          if (response.errorCode == "0") {
              callback(true, response);
          } else {
              callback(false, response);
          }
      })
      .catch(function (err) {
          console.log("get api.getMotorClmDtlsCatch listener is triggered");
          callback(false, err)
      });
});
